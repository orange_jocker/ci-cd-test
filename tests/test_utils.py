import unittest
import os
import sys
sys.path.append(os.getcwd())

from src.utils import *


class UtilsTester(unittest.TestCase):
    def test_add_fail_nans(self):
        vals = ['a', 1, 2]
        with self.assertRaises(TypeError):
            add(*vals)

    def test_add_success_ints(self):
        vals = [1, 2, 3]
        real_sum = 6
        self.assertEqual(add(*vals), real_sum)

    def test_add_success_floats(self):
        vals = [1., 2., 3.]
        real_sum = 6.
        self.assertAlmostEqual(add(*vals), real_sum)

    def test_always_true_success(self):
        self.assertTrue(always_true())

    def test_always_true_fail(self):
        pass
        # self.assertFalse(always_true())


if __name__ == "__main__":
    unittest.main(verbosity=2)
