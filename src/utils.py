"""
Simple module created only for testing purposes. Doesn't do anything useful
"""

def always_true():
    """Method that always returns true. Created for testing simpllicity."""
    return True


def add(*args):
    """Method, that, given several arguments, sums them up if possible."""
    return sum(args)
